\section{Basics}\label{Overview}

% This clearly needs some work. Pictures are too small.
% \begin{comment}
% \begin{figure}
%     \centering
%     \def\svgwidth{\columnwidth}
%     \input{HoTTCore.pdf_tex}% Manually changed _ -> \_
% \end{figure}
% \begin{figure}
%     \centering
%     \def\svgwidth{\columnwidth}
%     \input{HoTT.pdf_tex}% Manually changed _ -> \_
% \end{figure}  
% \end{comment}

We assume basic familiarity with homotopy type theory~\cite{hottbook}, and with the Coq
proof assistant~\cite{Coq}.
There is a large overlap between the contents of the HoTT library and the contents of the
book~\cite{hottbook}, which we refer to as the ``HoTT book''. The library provides an
automatically generated file linking the constructions in the book with the corresponding Coq
code.\footnote{\url{https://hott.github.io/HoTT/coqdoc-html/HoTTBook.html}}

\paragraph{Basic type formers and their identity types}

The core of the library is in the \texttt{Basics} and \texttt{Types}
directories.  These files formalize fundamental results about the higher
groupoid structure of identity types, and the identity types of basic types and type formers, such
as $0$, $1$, universes, $+$, $\times$, $\Pi$, and $\Sigma$.  This covers most of
Chapter~2 of the HoTT book, as well as parts of Chapters~3 and~7
(basic definitions and facts about $n$-types), Chapter~4 (equivalences; see below), and Chapter~5 (basic facts
about W-types).

The \texttt{Basics} directory contains absolutely basic facts
applicable to all types; while the \texttt{Types} directory is
organized with one file for each standard type former, roughly
matching the sections in Chapter~2 of the HoTT book.  Some other
basic facts from the first part of the HoTT book can be found in the
root \texttt{theories} directory, such as the comparison of different
definitions of equivalence (see below) and the proof that univalence
implies function extensionality.

\paragraph{Equivalences}

The HoTT book devotes most of Chapter~4 to discussing
various notions of equivalence. After showing that a large class of
them are equivalent, in a precise way, one can be agnostic on paper
about which is meant.  However, for a formalization we
need to choose a particular definition.

The intuitive notion of isomorphism or homotopy equivalence consists
of $f : A \to B$ and $g : B \to A$ which are inverses of each other,
up to homotopy.  However, in homotopy type theory the type of pairs
$f,g$ equipped with two such homotopies is ill-behaved, so one needs
to refine it somehow.

We have chosen to use the notion called a \emph{half-adjoint
  equivalence} in the HoTT book, which adds to this type a single
coherence condition between the two homotopies.  (The condition is one
of the triangle identities involved in an ``adjoint equivalence'' in
category theory; the other one is then provable, but should not be
assumed as data or the homotopy type would be wrong again.)  Since
this is ``the'' notion of equivalence in the library, we call it
simply an \emph{equivalence}.  Other possible options are Voevodsky's
definition of an equivalence as a map whose homotopy fibers are
contractible, or Andr\'e Joyal's suggestion of a map which has
separate left and right homotopy inverses.  We do prove the
equivalence of all these definitions (in
\texttt{EquivalenceVarieties}).

However, we believe that half-adjoint equivalences are a better choice
for `the' notion of equivalence in a formalization.  This is because
the most common way to construct an equivalence, and to use an
equivalence, is by way of the ``incoherent'' notion consisting of two
functions and two homotopies, called a \emph{quasi-inverse}
in the HoTT book, and half-adjoint equivalences record all
this data.  That is, usually what we care about when we have an
equivalence is that it has a homotopy inverse, and usually the way we
construct an equivalence is to exhibit a homotopy inverse and apply a
``coherentification'' result.  With half-adjoint equivalences
represented as a Coq record, all the data of a
quasi-inverse (plus the extra coherence) is stored exactly as supplied when an
equivalence is defined.  This applies in particular to the homotopy
inverse, but also to the witnessing homotopies, though the
``coherentification'' process alters one of these homotopies, so if we
want to preserve them both we have to manually prove the extra
coherence property.

\paragraph{Finite sets}
% https://github.com/HoTT/HoTT/blob/master/theories/Spaces/Finite.v
We define standard finite types \lstinline|Fin n| as usual,
%
\begin{lstlisting}
Fixpoint Fin (n : nat) : Type :=
  match n with
     | 0 => Empty
     | S n => Fin n + Unit
   end.
\end{lstlisting}
%
and then finite types as those that are merely equivalent to the
standard ones:
%
\begin{lstlisting}
Class Finite (X : Type) :=
  { fcard : nat ;
    merely_equiv_fin : merely (X <~> Fin fcard) }.
Definition issig_finite X
  : { n : nat & merely (X <~> Fin n) } <~> Finite X.  
\end{lstlisting}
Somewhat surprisingly, being finite is still a ``mere proposition'', because
canonical finite sets of different cardinalities are not isomorphic.  Thus, we could
have truncated the sigma and gotten an equivalent definition, but it
would be less convenient to reason about.


\paragraph{Pointed types}
\label{sec:pointed-types}

% https://github.com/HoTT/HoTT/blob/master/theories/Factorization.v
We provide a general theory of pointed types. The theory is
facilitated by a tactic which often allows us to pretend that pointed maps and homotopies preserve basepoints \emph{strictly}.  We have carefully defined pointed maps \lstinline|pMap| and pointed homotopies \lstinline|pHomotopy| so that when destructed, their second components are paths with right endpoints free, to which we can apply Paulin-Mohring path-induction. The theory of pointed types uses type
classes, since the base point can usually be found automatically:
\begin{lstlisting}
Class IsPointed (A : Type) := point : A.
Record pMap (A B : pType) :=
 { pointed_fun : A -> B ;
   point_eq : pointed_fun (point A) = point B }.
Record pHomotopy {A B : pType} (f g : pMap A B) :=
 { pointed_htpy: f == g ;
   point_htpy: 
     pointed_htpy (point A) @ point_eq g = 
     point_eq f }.
\end{lstlisting}

\paragraph{Category theory}

The library also includes a large development of category theory,
following Chapter~9 of the HoTT book and~\cite{ahrens2015univalent}. Details
about this part of the library were presented in~\cite{grosscat}, from which we
quote only the following:
\begin{quote}
  We wound up adopting the Coq
  version under development by homotopy type theorists, making critical use
  of its stronger universe polymorphism and higher inductive
  types\dots [which] can simplify the Coq user experience
  dramatically\dots
\end{quote}
The category theory library employs a different style of formalization
from the core library, using so-called ``blast'' tactics that
automatically try many lemmas to produce a proof by brute
force.  We avoid this approach in the core library to make proofs more
readable and give better control over proof terms (see ``Transparency
and Opacity'', below).

Timany and Jacobs~\cite{KULeuven-540221} provide another category
library building on top of our library for homotopy type theory.

\paragraph{Synthetic Homotopy theory}

The library also contains a variety of other
definitions and results, many relevant to
synthetic homotopy theory or higher category theory.  This includes
classifying spaces of automorphism groups, the Cantor
space, the theory of idempotents with the results of~\cite{shulman2016idempotents}, and the definition
of $\infty$-groups with actions. Such non-trivial additions to the core
provide strong evidence that the overall design is sustainable and usable.




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott"
%%% End:
