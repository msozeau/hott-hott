\section{Introduction}

Homotopy type theory is a novel approach to developing mathematics in Martin-L\"of’s type theory, based on interpretations of the theory into abstract
homotopy-theoretic settings such as higher toposes \cite{Rezk,Lurie}.
%
The connection between type theory and homotopy theory is originally due to \cite{AW} and \cite{VV}.

Identity types are interpreted as path spaces, and type equivalence as homotopy
equivalence. Type-theoretic constructions correspond to homotopy-invariant
constructions on homotopy types. In addition, homotopical intuition gives rise to entirely new
type-theoretic notions, such as higher inductive types and Voevodsky's univalence axiom.
One can even develop homotopy theory in the language of type theory in a ``synthetic''
manner, treating (homotopy) types as a primitive notion.

The first formalization of homotopy type theory in a proof assistant was Voevodsky's \emph{Foundations} library
implemented in Coq, now called the \emph{UniMath} project~\cite{UniMath}. Here we present the second such library, \emph{the HoTT library},
also implemented in Coq, with somewhat different goals from
those of UniMath. The library is freely available.\footnote{\url{https://github.com/HoTT/HoTT} or
the Coq OPAM package manager} 

% find . -iname "*.v" | xargs coqwc
Coq word count reports that the library contains 16800 lines of specifications, 13000
lines of proofs, and 4500 lines of comments. The library is self-sufficient and it completely
replaces the Coq standard library (which is incompatible with homotopy type theory) with a
bare minimum necessary for basic Coq tactics to function properly.

\paragraph{Contributions}
%
The HoTT library provides a substantive formalization of homotopy type theory. It
demonstrates that univalent foundations (cf.~\S\ref{Overview}) provide a workable setup
for formalization of mathematics. The library relies on advanced features of Coq
(cf.~\S\ref{sec:coq-features}), such as automatic handling of universe polymorphism
(cf.~\S\ref{univpoly}) and type classes (cf.~\S\ref{sec:type-classes}), management of
opaque and transparent definitions (cf.~\S\ref{sec:opaque}), and automation
(cf.~\S\ref{sec:automation}). We used private inductive types to implement higher
inductive types (cf.~\S\ref{HIT}), and the Coq module system to formalize modalities
(cf.~\S\ref{sec:modules}). Our development pushed Coq's abilities, which prompted the
developers to extend and modify it for our needs (cf.~\S\ref{sec:coq-changes}), and to
remove several bugs, for which we are most thankful. Overall, the success of the project
relies on careful policies and software-engineering approaches that keep the library
maintainable and usable (cf.~\S\ref{sec:software-engineering}). We relate our work to
other extensive implementations of homotopy type theory in~\S\ref{sec:related-work}.

\paragraph{Consistency}

A major concern for any piece of formalized mathematics is trust. The
HoTT library uses much more than just Martin-L\"of type theory,
including the univalence axiom, pattern matching, universe
polymorphism, type classes, private inductive types, second-class
modules, and so on; how do we know these form a consistent system?
This is a major concern not just for us, but for every user of complex
proof assistants, and it has been addressed many times in the past.

There are several possible ways to tackle safety.  So far, the primary
method available for homotopy type theory is semantic: to construct a
model of the theory in some other trusted theory (such as ZFC).  While
this has been done for many particular fragments of the theory,
combining them all to give a unified semantic account of homotopy type
theory together with Coq's extensions seems a daunting task.

For this reason, UniMath \cite{UniMath} avoids advanced Coq
technology, sticking as close as possible to standard Martin-L\"of
type theory (but nevertheless assuming the inconsistent
$\type\,{:}\,\type$ from the outset).
%
We feel rather that proof assistants and computerized formalization of mathematics are at such an
early stage that it is well worth experimenting, even at the risk of introducing an inconsistency
(which is fairly minor, due to the known semantic accounts of fragments of the theory).
In any case, the doubtful reader
should keep in mind that overall, the standard of proof in formalized mathematics is a great deal
higher than the generally accepted level of rigor among mathematicians.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott"
%%% End:
