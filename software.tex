\section{Technical changes to Coq}\label{sec:coq-changes}

During the development of the library we had close contacts with the
Coq developers, most notably Matthieu Sozeau. A number of changes to the Coq
system were needed when we started, but today the library compiles with standard Coq~8.5.1 and later.
Our library serves as a testing platform for a number of new Coq features. Inefficiencies and bugs are
reported by us and quickly addressed by the Coq
developers. We mention several noteworthy changes to Coq:
\begin{itemize}
\item The inductive definition of the
identity type has a single constructor, and so Coq puts it in $\prop$, contrary to what is needed in homotopy type theory.
The \texttt{indices-matter} option of Coq, which was implemented already by Hugo Herbelin for the purposes of the
Foundations library~\cite{VV-foundations}, changes this behavior to the desired one.
\item We avoid the impredicative $\prop$ altogether and only use $\hprop$. An
element of $\hprop$ consists of a type together with a proof that the type has at most one element.
This small change makes the whole standard library unusable, and
many tactics stop working, too. The solution was rather drastic: we ripped out the standard library
and replaced it with a minimal core that is sufficient for the basic tactics to work. There is experimental work
that aims to disentangle the tactics and the standard libary,\footnote{\url{https://github.com/ejgallego/coq/tree/coqlib-cleanup-master}}
which we hope to use in the future.
\item The private inductive types are another experimental addition to Coq which allowed us to
  implement higher inductive types. This was already discussed in \S\ref{HIT}.
\end{itemize}


\section{Software Engineering}\label{sec:software-engineering}

The collaborative development of the library was made possible by
using modern software engineering tools:
\begin{itemize}
\item We use GitHub as a platform for git version control and discussion of the code. We
  have a strict `two pairs of eyes' policy according to which a code change may be
  accepted only after it has been reviewed by two other developers. This facilitates good
  collaboration and the use of standardized naming schemes.
\item An extensive style guide facilitates collaboration and allows
  others to contribute and build on the library. We use the GitHub wiki to keep track of 
 documentation, and we automatically generate browsable and replayable literate code with Coqdoc
 and Proviola\footnote{\url{http://mws.cs.ru.nl/proviola/}} tools.
  Our library is also one
 of the test cases for the JavaScript interface for Coq.\footnote{\url{https://x80.org/rhino-hott/}}
\item We use Travis\footnote{\url{https://travis-ci.org/}} for
  continuous integration. It checks
  whether a proposed code change compiles, which serves as a very useful sanity check. Travis also allowed us to keep track
  of compilation time, which may be an issue with such a large library. We built tools that help identify the cause
  of degrade performance. When the culprit was a Coq change, we reported it to Coq developers who quickly fixed the issue.
\item The installation procesure for the HoTT library is fairly complicated, as it requires a customized Coq installation.
  We provide automated scripts that help users with the installation. We also made the library available through the
  very successful OCaml Package Manager (OPAM).
\item The use of Proof General together with Emacs TAGS allows us to
  easily navigate the library and find definitions of
  terms; \texttt{company-coq} provides even better facilities.
\end{itemize}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott"
%%% End:

